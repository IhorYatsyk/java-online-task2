package com.epam;

import java.util.Scanner;
import java.util.Random;

public class Rumors {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the integer N count of guests.");
        System.out.print("The count of guests N = ");
        int countOfGuests = sc.nextInt();
        while(countOfGuests <= 0) {
            System.out.println("The count N of guests isn't positive! Enter the positive count of guests.");
            System.out.print("The count of guests N = ");
            countOfGuests = sc.nextInt();
        }
        if (countOfGuests <= 3){System.out.println("Probability = 1");
            System.out.println("Expected number of people = " + Integer.toString(countOfGuests + 1));}//Add Alice
        else {
            boolean [] guestHeardRumor = new boolean [countOfGuests];
            for (int i = 0; i < countOfGuests; i++){
                if (i <= 2){guestHeardRumor[i] = true;}//Bob and two others
                else {guestHeardRumor[i] = false;}//except Bob and two others
            }
            int countOfRumorsHeard = 3;// Minimum count of guests who heard rumor is three: Bob and two others
            Random random = new Random();
            int nextNumberHeardRumor = random.nextInt(countOfGuests) + 1;
            while (countOfRumorsHeard <= countOfGuests && (guestHeardRumor[nextNumberHeardRumor-1] == false) &&
                    nextNumberHeardRumor != countOfRumorsHeard && nextNumberHeardRumor != countOfRumorsHeard - 1) {
                guestHeardRumor[nextNumberHeardRumor-1] = true;
                countOfRumorsHeard = countOfRumorsHeard + 1;
                nextNumberHeardRumor = random.nextInt(countOfGuests) + 1;
        }
            float probability = (float)countOfRumorsHeard/countOfGuests;
            System.out.println("Probability = " + Float.toString(probability));
            System.out.println("Expected number of people = " + Integer.toString(countOfRumorsHeard));
        }


    }
}
